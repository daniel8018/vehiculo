package daniel.rivera.bl;

public class Motor {
    private String serie;
    private int cilindros;

    /**
     * Constructor vacio para la clase Motor
     */
    public Motor() {
    }

    /**
     * Constructor que recibe todos los parámetros de  Motor y los Inicializa
     * @param serie Variable que simboliza la serie del motor
     * @param cilindros Variable que simboliza los cilindros de la clase Cilindros
     */
    public Motor(String serie, int cilindros) {
        this.serie = serie;
        this.cilindros = cilindros;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada serie de la clase
     * @return la variable de retorno serie simboliza la serie del motor
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Serie de la clase Motor
     * @param serie Variable que simboliza la de la clase motor
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Cilindros de la clase
     * @return la variable de retorno simboliza los cilindros del motor
     */
    public int getCilindros() {
        return cilindros;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Cilindros de la clase Motor
     * @param cilindros Variable que simboliza cilindros de la clase Motor
     */
    public void setCilindros(int cilindros) {
        this.cilindros = cilindros;
    }

     /**
          * Metodo utilizado para imprimir todos los atributos de la clase Motor en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase Motor en unico string
          */
    @Override
    public String toString() {
        return "Motor{" +
                "serie='" + serie + '\'' +
                ", cilindros=" + cilindros +
                '}';
    }
}
