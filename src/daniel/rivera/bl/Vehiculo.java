package daniel.rivera.bl;

public class Vehiculo {
private String placa,marca,color;
private Motor motor;

    /**
     * Constructor vacio para la clase Vehiculo
     */
    public Vehiculo() {
    }

    /**
     * constructor que inicializa todos los parámetros de la clase vehiculo
     * @param placa Variable que simboliza la placa del vehiculo de la clase vehiculo
     * @param marca Variable que simboliza la marca del vehiculo de la clase vehiculo
     * @param color Variable que simboliza  el color del vehiculo de la clase vehiculo
     */
    public Vehiculo(String placa, String marca, String color) {
        this.placa = placa;
        this.marca = marca;
        this.color = color;
    }

    /**
     * Constructor que inicializa la agregación de motor con vehiculo
     * @param placa Variable que simboliza la placa del vehiculo de la clase vehiculo
     * @param marca marca Variable que simboliza la marca del vehiculo de la clase vehiculo
     * @param color Variable que simboliza  el color del vehiculo de la clase vehiculo
     * @param motor Variable que simboliza  el motor del vehiculo de la clase Motor
     */
    public Vehiculo(String placa, String marca, String color, Motor motor) {
        this.placa = placa;
        this.marca = marca;
        this.color = color;
        this.motor = motor;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada placa de la clase Vehiculo
     * @return  la variable de retorno simboliza la placa del vehiculo
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * Metodo utilizado para modificar el atributo privado placa de la clase Vehiculo
     * @param placa Variable que simboliza la placa de la clase vehiculo
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Marca de la clase
     * @return la variable de retorno simboliza la marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Metodo utilizado para modificar el atributo privado marca de la clase
     * @param marca Variable que simboliza la marca de la clase vehiculo
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada color de la clase vehiculo
     * @return la variable de retorno simboliza el color del vehiculo
     */
    public String getColor() {
        return color;
    }

    /**
     * Metodo utilizado para modificar el atributo privado color de la clase vehiculo
     * @param color Variable que simboliza el color del vehiculo
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Motor de la clase vehiculo
     * @return
     */
    public Motor getMotor() {
        return motor;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Motor de la clase vehiculo
     * @param motor la variable de retorno simboliza el motor
     */
    public void setMotor(Motor motor) {
        this.motor = motor;
    }

     /**
          * Metodo utilizado para imprimir todos los atributos de la clase Vehiculo en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase - en unico string
          */
    public String toList() {
        return "Vehiculo{" +
                "placa='" + placa + '\'' +
                ", marca='" + marca + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

 /**
      * Metodo utilizado para imprimir todos los atributos de la clase Vehiculo en un unico String
      * @return la variable de retorno simboliza todos los valores de la clase - en unico string
      */
    @Override
    public String toString() {
        return "Vehiculo{" +
                "placa='" + placa + '\'' +
                ", marca='" + marca + '\'' +
                ", color='" + color + '\'' +
                ", motor=" + motor +
                '}';
    }
}

