package daniel.rivera.dl;

import daniel.rivera.bl.Motor;
import daniel.rivera.bl.Vehiculo;

import java.util.ArrayList;

public class CapaLogica {
    private ArrayList<Vehiculo> cars;
    private ArrayList<Motor> motors;

    /**
     * Constructor que recibe todos los parámetros del ArraList y los Inicializa
     */
    public CapaLogica() {
        cars = new ArrayList<>();
        motors = new ArrayList<>();

    }
    //_______________________________Agregar___________________________________

    /**
     * metodo utilizado para registrar el objeto vehiculo y ser enviado al ArrayList
     * @param vehiculo Variable que simboliza el objeto vehiculo de la clase Vehiculo
     */
    public void agregarVehiculo(Vehiculo vehiculo) {
        cars.add(vehiculo);
    }

    /**
     * metodo utilizado para registrar el objeto Motor y ser enviado al ArrayList
     * @param motor Variable que simboliza el objeto motor de la clase Motor
     */
    public void agregarMotor(Motor motor){
        motors.add(motor);
    }

    //__________________________________________________________________
//__________________________________listar________________________________

    /**
     * Metodo utilizado para listar los vehiculos de la clase Vehiculos
     * @return la variable de retorno simboliza la informacion de los vehiculos
     */
    public String[] listarVehiculos() {
        String[] infovehiculos = new String[cars.size()];
        for (int i = 0; i < cars.size(); i++) {
            infovehiculos[i]=cars.get(i).toList();
        }
        return infovehiculos;
    }

    /**
     * Metodo utilizado para listar los Motores de la clase Motor
     * @return la variable de retorno simboliza la informacion de los Motores
     */
    public String[] listarMotores(){
     String[] infomotores = new String [motors.size()];
     for (int i=0; i<motors.size();i++){
         infomotores[i]=motors.get(i).toString();
        }
        return infomotores;
    }

    /**
     * Metodo utilizado para listar la Asociacion de la clase Vehiculos con Motores
     * @return la variable de retorno simboliza la informacion de los vehiculos y motores
     */
    public String[] listarVehiculosMotores() {
        String[] infovehiculosMotores = new String[cars.size()];
        for (int i = 0; i < cars.size(); i++) {
            infovehiculosMotores[i] = cars.get(i).toString();
        }
        return infovehiculosMotores;
    }

//__________________________________________________________________

    //______________________Buscar_______________________

    /**
     * método utilizado para verificar si el motor
     * @param serie
     * @return
     */
    public int buscarMotor(String serie){
        int posicion = 0;
        for(Motor check :motors){
            if(serie.equals(check.getSerie())){
                return posicion;
            }
            posicion ++;
        }
        return -1;
    }

    public int buscarVehiculo(String placa){
        int posicion = 0;
        for(Vehiculo check :cars){
            if(placa.equals(check.getPlaca())){
                return posicion;
            }
            posicion ++;
        }
        return -1;
    }
    //______________________________________________________________

    //__________________________________ASOCIAR________________________________
    public void asociarVehiculoMotor(int posVehiculo, int posMotor) {
        Motor motor = motors.get(posMotor);
        cars.get(posVehiculo).setMotor(motor);
    }//FIN ASOCIAR
    //__________________________________________________________________

}//CAPALOGICA
