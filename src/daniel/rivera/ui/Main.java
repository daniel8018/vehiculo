package daniel.rivera.ui;

import daniel.rivera.bl.Motor;
import daniel.rivera.bl.Vehiculo;
import daniel.rivera.dl.CapaLogica;
import daniel.rivera.tl.Controller;

import java.io.*;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller administrador = new Controller();
    static CapaLogica logica = new CapaLogica();

    public static void main(String[] args) throws IOException {
        mostrarMenu();
    }

    //______________________________registrar____________________________________
    public static void registrarMotor() throws IOException {
        out.println("Digite la serie del motor");
        String serie = in.readLine();
        out.println("Digite los cilindros del motor");
        int cilindros = Integer.parseInt(in.readLine());
        Motor motor = new Motor (serie,cilindros);
        if (administrador.buscarMotorController(serie) == -1) {
            administrador.registrarMotor(serie,cilindros);
            out.println("El motor se ha registrado correctamente");
        } else {
            out.println("\"El motor serie: " + serie + " ya existe.");
        }
    }


    public static void registrarVehiculo() throws IOException {
        out.println("Digite la placa del vheiculo");
        String placa = in.readLine();
        out.println("Digite la marca del vehiculo");
        String marca = in.readLine();
        out.println("Digite el color del vehiculo");
        String color = in.readLine();
        Vehiculo car = new Vehiculo(placa,marca,color);
        if (administrador.buscarVehiculoController(placa) == -1) {
            administrador.registrarVehiculo(placa,marca,color);
            out.println("El Vehiculo se ha registrado correctamente");
        } else {
            out.println("\"El Vehiculo placa:" + placa + " ya existe.");
        }
    }//FIN registrarVehiculo

    public static void registrarVehiculoConMotor() throws IOException {
        out.println("Digite la placa del vheiculo");
        String placa = in.readLine();
        out.println("Digite la marca del vehiculo");
        String marca = in.readLine();
        out.println("Digite el color del vehiculo");
        String color = in.readLine();
        out.println("Digite la serie del motor");
        String serie = in.readLine();
        out.println("Digite los cilindros del motor");
        int cilindros = Integer.parseInt(in.readLine());
        administrador.registrarVehiculoConMotor(placa, marca, color, serie, cilindros);
    }//FIN registrarVxM
    //__________________________________________________________________
//______________________________LISTAR____________________________________
    public static void mostrarVehiculos() {
        String[] infoVehiculo = administrador.listarVehiculos();
        for (int i = 0; i < infoVehiculo.length; i++) {
            out.println(infoVehiculo[i]);
        }
    }
//*****************************************************************
  public static void mostrarMotores() {
     String[] infoMotor = administrador.listarMotores();
     for (int i = 0; i < infoMotor.length; i++) {
         out.println(infoMotor[i]);
     }
 }
 //***************************************************************
 public static void mostrarVehiculosMotores() {
     String[] infoVehiculoMotores = administrador.listarVehiculosMotores();
     for (int i = 0; i < infoVehiculoMotores.length; i++) {
         out.println(infoVehiculoMotores[i]);
     }
 }
//__________________________________________________________________

    //__________________________________________________________________
    public static void asociarVehiculoMotor() throws IOException{
        out.print("Ingrese el número de serie Del motor: ");
        String serie = in.readLine();
        out.print("Digite el número de Placa del vehículo: ");
        String placa = in.readLine();

        if(administrador.asociarVehiculoMotor(serie, placa)){
            out.println("Se asoció el Motor al vehículo de manera correcta");
        } else {
            out.println("No se pudo asociar el vehiculo al motor, debido a que alguno de los 2 no existe");
        }
    }
    //__________________________________________________________________
//____________________________________MENU_________________________________________________
    static void mostrarMenu() throws IOException {
        int opcion = -1;
        do {
            System.out.println("Menú");
            System.out.println("1. Registrar Motor");
            System.out.println("2. Listar Motor");
            System.out.println("3. Registrar Vehiculo");
            System.out.println("4. Listar Vehiculo");
            System.out.println("5. Registrar Vehiculo Y MOTOR");
            System.out.println("6. Listar Vehiculo Y MOTOR");
            System.out.println("0. Salir");
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);
    }

    /**
     * Rutina (función) que retorna el valor ingresado por el usuario.
     *
     * @return devuelve la opción seleccionada
     */
    static int seleccionarOpcion() throws IOException {
        System.out.println("Digite la opción");
        return Integer.parseInt(in.readLine());

    }

    //Rutina (procedimiento) que según el parámetro, llama al proceso respectivo.
    static void procesarOpcion(int pOpcion) throws IOException {

        switch (pOpcion) {
            case 0:
                System.out.println("¡Aplicación cerrada exitosamente!.");
                break;
            case 1:
                registrarMotor();
                break;
            case 2:
                mostrarMotores();
                break;
            case 3:
                registrarVehiculo();
                break;
            case 4:
                mostrarVehiculos();
                break;
            case 5:
                asociarVehiculoMotor();
                break;
            case 6:
                mostrarVehiculosMotores();
                break;
            default:
                System.out.println("Opción inválida");
                break;
        }
    }
    //__________________________________________________________________


}
