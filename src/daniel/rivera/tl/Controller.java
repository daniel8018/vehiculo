package daniel.rivera.tl;

import daniel.rivera.bl.Motor;
import daniel.rivera.bl.Vehiculo;
import daniel.rivera.dl.CapaLogica;

public class Controller {

    private CapaLogica logica = new CapaLogica();

    /**
     * metodo utilizado para registrar el objeto vehiculo y ser enviado a la capa logica
     * @param placa Variable que simboliza la placa del vehiculo de la clase vehiculo
     * @param marca Variable que simboliza la marca del vehiculo de la clase vehiculo
     * @param color Variable que simboliza  el color del vehiculo de la clase vehiculo
     * @param serie Variable que simboliza la serie de la clase Motor
     * @param cilindros Variable que simboliza los cilindros de la clase Motor
     */
    public void registrarVehiculoConMotor(String placa, String marca, String color, String serie, int cilindros) {
     Motor nuevoMotor = new Motor(serie,cilindros);
     Vehiculo carro = new Vehiculo(placa,marca,color,nuevoMotor);
     logica.agregarVehiculo(carro);
    }


    //________________________________LISTAR__________________________________

    /**
     *Metodo utilizado para listar los motores de la clase Motor
     * @return la variable de retorno simboliza el string enviado a la logica
     */
    public String[] listarMotores(){
        return logica.listarMotores();
    }

    /**
     *Metodo utilizado para listar los Vehiculos de la clase Vehiculo
     * @return la variable de retorno simboliza el string enviado a la logica
     */
    public String[] listarVehiculos(){
        return logica.listarVehiculos();
    }

    /**
     *Metodo utilizado para listar los motores de la clase Motor
     * @return la variable de retorno simboliza el string enviado a la logica
     */
    public String[] listarVehiculosMotores(){
        return logica.listarVehiculosMotores();
    }

    //____________________________VEHICULO____________________________________

    /**
     * Metodo utilizado para buscar el String placa en el ArrayList en la logica
     * @param placa Variable que simboliza la placa de la clase Vehiculo
     * @return
     */
    public int buscarVehiculoController(String placa) {
        int encontrado = logica.buscarVehiculo(placa);
        return encontrado;
    }

    /**
     * Metodo utilizado para buscar el String serie en el ArrayList en la logica
     * @param serie Variable que simboliza la serie de la clase Motor
     * @return la variable de retorno simboliza
     */
    public int buscarMotorController(String serie) {
        int encontrado = logica.buscarMotor(serie);
        return encontrado;
    }

    //__________________________________________________________________

    //_________________________________registrar__________________________


    /**
     * metodo utilizado para registrar el objeto carro y ser enviado a la capalogica
     *@param placa Variable que simboliza la placa del vehiculo de la clase vehiculo
      * @param marca Variable que simboliza la marca del vehiculo de la clase vehiculo
     * @param color Variable que simboliza  el color del vehiculo de la clase vehiculo
     */
    public void registrarVehiculo(String placa, String marca, String color) {
        Vehiculo carro = new Vehiculo(placa, marca, color);
         logica.agregarVehiculo(carro);
    }

    /**
     * metodo utilizado para registrar el objeto Motor y ser enviado a la capalogica
     *@param serie Variable que simboliza la serie del motor de la clase motor
     * @param cilindro Variable que simboliza la cantidad de cilindros del vehiculo de la clase cilindro
     */
    public void registrarMotor(String serie, int cilindro) {
        Motor motor = new Motor(serie, cilindro);
        logica.agregarMotor(motor);
    }
    //______________________________________________________________________

    //____________________________________ASOCIAR_____________________________

    /**
     * metodo utilizado para registrar el vehiculo con el motor, obteniendo la serie y la placa
     * @param serie este parámetro se utiliza para ser utilizado en la busqueda
     * @param placa este parámetro se utiliza para ser utilizado en la busqueda
     * @return devuelve un booleano para saber si el arreglo está lleno
     */
    public boolean asociarVehiculoMotor(String serie, String placa) {
        int posMotor = logica.buscarMotor(serie);
        if (posMotor >= 0) {
            int posVehiculo = logica.buscarVehiculo(placa);
            if (posVehiculo >= 0) {
                logica.asociarVehiculoMotor(posVehiculo, posMotor);
                return true;
            }
        }
        return false;

    }//FIN ASOCIAREMPLEADO
    //__________________________________________________________________

}//FIN CONTROLLER